import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { CategoryChartColumnChartMultipleSourcesComponent } from "./column-chart-multiple-sources/category-chart-column-chart-multiple-sources.component";
// import { 
// 	IgxCategoryChartModule,
// 	IgxLegendModule
//  } from "igniteui-angular-charts";

import { AnalyticsRoutingModule } from './analytics-routing.module';
import { AnalyticsComponent } from './analytics.component';


@NgModule({
  declarations: [AnalyticsComponent],
  imports: [
    CommonModule,
    AnalyticsRoutingModule
  ]
})
export class AnalyticsModule { }
