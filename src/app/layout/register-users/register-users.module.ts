import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterUsersRoutingModule } from './register-users-routing.module';
import { RegisterUsersComponent } from './register-users.component';


@NgModule({
  declarations: [RegisterUsersComponent],
  imports: [
    CommonModule,
    RegisterUsersRoutingModule
  ]
})
export class RegisterUsersModule { }
