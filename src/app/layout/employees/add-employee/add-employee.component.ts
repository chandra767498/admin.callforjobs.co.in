import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import swal from 'sweetalert2';
import { AppService } from 'src/app/app.service';
@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {
  hide = true;
employeeData: any = {};
registrationData: any = {};
permissionsData: any = {};
adminData: any;
paramsData: any = {};
isShown = false;
showMe = true;
optionValue = '';
optionalValue = '';
isEdit: any = {};
is_admin = '';
role = '';
isAdminListLoadin: boolean;
LanguageList: any;
closeResult: string;
languageData = [];
employee_new_id: any = {};
addLanguageData: any = {};
DeleteForm: FormGroup;
delete_employee_data: any = {};
delete_employee: any = {};
language_id;
admin_is = false;
email_id = '';
employee_id;
roles = {};

  constructor(
    private router: Router,
    private appService: AppService,
    private cookieService: CookieService,
  ) {
    if (this.cookieService.get('adminData')) {
        this.adminData = JSON.parse(this.cookieService.get('adminData'));
    } else {
        this.adminData = {};
        // this.router.navigate(['/login']);
    }
    // const navigation = this.router.getCurrentNavigation();
    // const state = navigation.extras as {
    //     title: string;
    //     id: BigInteger;
    // };
    // console.log('>>>>>>12', state);

    // this.activateEdit(state);

}

activateEdit(state) {
    this.registrationData.name = state.name;
    this.language_id = state.language_id;
    this.admin_is = state.is_admin;
    this.permissionsData = state.permissions;
    this.email_id = state.email_id;
    this.employee_id = state.id;
}

  ngOnInit(): void {
        this.paramsData.admin_id = this.adminData.id;
        this.paramsData.session_id = this.adminData.session_id;
  }

create() {
    // JSON.parsethis.cookieService.get('adminData');
    // const adminData = JSON.parse(this.cookieService.get('adminData'));
    const body = {
      admin_id: this.paramsData.admin_id,
      session_id: this.paramsData.session_id,
      name: this.registrationData.name,
      email_id: this.registrationData.email_id,
      pwd: this.registrationData.pwd,
      is_admin: this.registrationData.experience === 'true' ? true : false,
      permissions: this.permissionsData
    };
    console.log(body);
    try {
      this.appService.postMethod('Create_Admin_User', body)
        .subscribe((resp: any) => {
          if (resp.success) {
            swal.fire('Employee Created Succesfully');

          } else {
            swal.fire('Enter All Tags', 'Something went wrong!', 'error');
          }
        },
          error => {
          });
    } catch (e) { }
  }

}
