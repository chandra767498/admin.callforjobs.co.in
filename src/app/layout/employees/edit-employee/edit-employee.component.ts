import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import swal from 'sweetalert2';
import { AppService } from 'src/app/app.service';
@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.scss']
})
export class EditEmployeeComponent implements OnInit {
  adminData: any;
  paramsData: any = [];
  email_id: '';
  employee_id;
  editData: any = {};
  is_admin = '';
  hide = true;
  registrationData: any = {};
  permissionsData: any = {};
  item: any = {};
  employeeData: any = {};
  constructor(
    private router: Router,
    private appService: AppService,
    private cookieService: CookieService,
  ) {
    if (this.cookieService.get('adminData')) {
      this.adminData = JSON.parse(this.cookieService.get('adminData'));
  }
}

  ngOnInit(): void {
    this.paramsData.admin_id = this.adminData.id;
    this.paramsData.session_id = this.adminData.session_id;
  }
  editForm() {
    const data = {
        admin_id: this.paramsData.admin_id,
        session_id: this.paramsData.session_id,
        email_id: this.email_id,
        name: this.registrationData.name,
        // employee_id: this.item.id,
        permissions: this.permissionsData

    };
    console.log(data);
    try {
      this.appService.postMethod('edit_employee', data).subscribe(
          (resp: any) => {
              if (resp.sucess) {
                  swal.fire(resp.msg, '');
                  this.router.navigate(['/employees/list-employees']);
              } else {
                  swal.fire(resp.msg, '');
                  this.router.navigate(['/employees/list-employees']);

              }
          },
          (error) => {}
      );
  } catch (e) {}
}
}
