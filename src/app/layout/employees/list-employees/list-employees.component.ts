import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from 'src/app/app.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import {
  NgbModal,
  NgbModalConfig,
  ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import { PageEvent } from '@angular/material/paginator';

import swal from 'sweetalert2';
@Component({
  selector: 'app-list-employees',
  templateUrl: './list-employees.component.html',
  styleUrls: ['./list-employees.component.scss']
})
export class ListEmployeesComponent implements OnInit {
  items: any = [];
  paramsData: any = [];
  adminData: any;
  language_id;
  isEdit: any = {};
  optionValue = '';
  optionalValue = '';
  email_id: '';
  pageEvent: PageEvent;
  approveskip = 0;
  limit = 10;
  skip = 0;
  length: number;
  employee_id;
  registrationData: any = {};
  currentPage_approve: any = {};
  permissionsData: any = {};
  delete_employee_data: any = {};
  delete_employee: any = {};
  closeResult: string;
  pageIndex: number;
  pageSize: number;
  constructor(
    private router: Router,
    private appService: AppService,
    private cookieService: CookieService,
    private modalService: NgbModal,

    ) {  if (this.cookieService.get('adminData')) {
      this.adminData = JSON.parse(this.cookieService.get('adminData'));
    }
  }
  ngOnInit(

  ): void {
    this.paramsData.admin_id = this.adminData.id;
    this.paramsData.session_id = this.adminData.session_id;
    // this.List_All_Languages(this.paramsData);
    this.List_All_Employees(0);
}

List_All_Employees(skip) {
  // console.log(this.employeeData);
  const adminData: any = JSON.parse(this.cookieService.get('adminData'));
  const data = {
    admin_id: this.paramsData.admin_id,
    session_id: this.paramsData.session_id,
    skip: skip,
    limit: 10,
  };
  try {
    this.appService.postMethod('List_All_Employees', data).subscribe(
      (resp: any) => {
        if (resp.success) {
          this.items = resp.data;
          // this.length = resp.count;
          this.length = resp.count;
          console.log(resp);
        } else {
          swal.fire(resp.msg, 'error');
        }
      },
      (error) => { }
    );
  } catch (e) { }
}


Active_Inactive_Employee(item) {
  const adminData: any = JSON.parse(this.cookieService.get('adminData'));
  const body = {
      admin_id: adminData.id,
      session_id: adminData.session_id,
      employee_id: item.id,
      status: 2,
  };
  try {
      this.appService.postMethod('Active_Inactive_Employee', body).subscribe(
          (resp) => {
              if (resp.success) {
                  this.List_All_Employees(0);
              } else {
              }
          },
          (error) => {}
      );
  } catch (e) {}
}


delarticle(item) {

  swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
  }).then((result) => {

      if (result.value) {
        this.Active_Inactive_Employee(item);
          swal.fire(
              'Deleted!',
              'Your Employee deleted Sucessfully.',
              'success'
          );
          // this.delForm();
      } else if (result.dismiss === swal.DismissReason.cancel) {
          swal.fire('Cancelled', 'Your Employee is safe :)', 'error');
      }
  });
}

onNextPage(event) {
  this.currentPage_approve = event;
  this.approveskip = event.pageIndex * 10;
  console.log(this.approveskip, 'Skip');
  this.List_All_Employees(this.approveskip);
  return event;
}
EditEmployee(event , item) {
  console.log(item);
  this.router.navigate(['employees/edit-employee'], item);
}
onEditClick(event) {
  console.log(event, '<<<<');
  this.optionValue = event;
}
onEditClickk(event) {
  console.log(event, '<<<<');
  this.optionalValue = event;
}
del(content, data) {
  this.delete_employee_data = data,
    this.popupCalls(content);
}

popupCalls(content) {
  console.log('hllo', content);
  this.modalService.open(content).result.then(
    (result) => {
      this.closeResult = `Closed with: ${result}`;
      console.log('in close', content);
    },
    (reason) => {
      console.log('in dismis');
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    }
  );
}

private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return `with: ${reason}`;
  }
}
}
