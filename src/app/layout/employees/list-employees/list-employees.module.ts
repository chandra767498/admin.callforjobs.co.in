import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ReactiveFormsModule} from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatSelectModule} from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { ListEmployeesRoutingModule } from './list-employees-routing.module';
import { ListEmployeesComponent } from './list-employees.component';

@NgModule({
  declarations: [ListEmployeesComponent],
  imports: [
    CommonModule,
    ListEmployeesRoutingModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    TranslateModule,
    MatPaginatorModule
  ]
})
export class ListEmployeesModule { }
