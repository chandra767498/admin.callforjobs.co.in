import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeesComponent } from './employees.component';


const routes: Routes = [
{  
    path: '',
    redirectTo: 'add-employee',
},
{
    path: 'add-employee',
    loadChildren: () =>
        import('./add-employee/add-employee.module').then(
            (m) => m.AddEmployeeModule
        ),
},
{
    path: 'list-employees',
    loadChildren: () =>
        import('./list-employees/list-employees.module').then((m) => m.ListEmployeesModule),
},
{
    path: 'edit-employee',
    loadChildren: () =>
        import('./edit-employee/edit-employee.module').then((m) => m.EditEmployeeModule),
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeesRoutingModule { }
