import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';
const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
            },
            {
                path: 'languages',
                loadChildren: () => import('./languages/languages.module').then(m => m.LanguagesModule)
            },
            {
                path: 'static-data',
                loadChildren: () => import('./static-data/static-data.module').then(m => m.StaticDataModule)
            },

            {
                path: 'employees',
                loadChildren: () => import('./employees/employees.module').then(m => m.EmployeesModule)
            },
            {
                path: 'register-users',
                loadChildren: () => import('./register-users/register-users.module').then(m => m.RegisterUsersModule)
            },
            {
                path: 'user-role',
                loadChildren: () => import('./user-role/user-role.module').then(m => m.UserRoleModule)
            },
            {
                path: 'job-section',
                loadChildren: () => import('./job-section/job-section.module').then(m => m.JobSectionModule)
            },
            {
                path: 'setting',
                loadChildren: () => import('./setting/setting.module').then(m => m.SettingModule)
            },
            {
                path: 'job-posting',
                loadChildren: () => import('./job-posting/job-posting.module').then(m => m.JobPostingModule)
            },
            {
                path: 'analytics',
                loadChildren: () => import('./analytics/analytics.module').then(m => m.AnalyticsModule)
            },
        ],
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule { }
