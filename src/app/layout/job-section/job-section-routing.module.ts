import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobSectionComponent } from './job-section.component';


const routes: Routes = [{
 
    path: '',
    redirectTo: 'job-category',
},
{
    path: 'job-category',
    loadChildren: () =>
        import('./job-category/job-category.module').then(
            (m) => m.JobCategoryModule
        ),
},
{
    path: 'company',
    loadChildren: () =>
        import('./company/company.module').then(
            (m) => m.CompanyModule
        ),
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobSectionRoutingModule { }
