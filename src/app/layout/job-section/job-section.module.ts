import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobSectionRoutingModule } from './job-section-routing.module';
import { JobSectionComponent } from './job-section.component';


@NgModule({
  declarations: [JobSectionComponent],
  imports: [
    CommonModule,
    JobSectionRoutingModule
  ]
})
export class JobSectionModule { }
