import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import {
    NgbModal,
    NgbModalConfig,
    ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import { PageEvent } from '@angular/material/paginator';
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    ValidatorFn,
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from 'src/app/app.service';
import swal from 'sweetalert2';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {
  editData: any = {};
  languageData: any = {};
  companiesData: any = {};
  data: any = {};
  company: any = {};
  postDataView: any = {};
  adminData: any;
  closeResult: string;
  postdataShow: boolean;
  safeSrc: SafeResourceUrl;
  companyData: any = {};
  pageEvent: PageEvent;
  approveskip = 0;
  currentPage_approve: any = {};
  limit = 10;
  skip = 0;
  length: number;
  jobData: any = {};
  paramsData: any = {};
  delete_employee_data: any = {};
  delete_employee: any;
  constructor(
    private sanitizer: DomSanitizer,
    private modalService: NgbModal,
    private appService: AppService,
    private cookieService: CookieService,
    private router: Router

  ) {
    if (this.cookieService.get('adminData')) {
      this.adminData = JSON.parse(this.cookieService.get('adminData'));
   }
   }

  ngOnInit(): void {
    this.companyData.status = 'active';
    this.paramsData.admin_id = this.adminData.id;
    this.paramsData.session_id = this.adminData.session_id;
    this.List_All_Companies(0);
  }
  submitForm() {
    const data = {
      admin_id : this.paramsData.admin_id,
      session_id: this.paramsData.session_id,
        name: this.companiesData.name,
        category: this.companiesData.category,
        mobile: this.companiesData.mobile,
        url: this.companiesData.url,
    };
    console.log(data);
    try {
        this.appService.postMethod('Add_company', data).subscribe(
            (resp: any) => {
                if (resp.success) {
                    this.companiesData = resp.msg;
                    swal.fire('Company Added Successfully');
                    this.List_All_Companies(0);
                    console.log(resp);
                } else {
                    swal.fire(resp.msg, '');
                }
            },
            (error) => {}
        );
    } catch (e) {}
}

List_All_Companies(skip) {
  // console.log(this.employeeData);
  const data = {
    admin_id: this.paramsData.admin_id,
    session_id: this.paramsData.session_id,
    skip: skip,
    limit: 10,
  };
  try {
    this.appService.postMethod('List_All_Companies', data).subscribe(
      (resp: any) => {
        if (resp.success) {
          this.companyData = resp.data;
          this.length = resp.count;
          console.log(resp);
        } else {
          swal.fire(resp.msg, 'error');
        }
      },
      (error) => { }
    );
  } catch (e) { }
}
delcompany(company) {
  swal.fire({
    title: 'Are you sure?',
    text: 'You will not be able to recover the Company',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, keep it',
  }).then((result) => {
    if (result.value) {
      this.delete_company(company);
      swal.fire(
        'Deleted!',
        'Your Company deleted Sucessfully.',
        'success'
      );
    } else if (result.dismiss === swal.DismissReason.cancel) {
      swal.fire('Cancelled', 'Your Company is safe :)', 'error');
    }
  });
}

editForm() {
  const data = {
    admin_id: this.paramsData.admin_id,
    session_id: this.paramsData.session_id,
  };
  // try {
  //   this.appService.postMethod('edit_employee', data).subscribe(
  //     (resp: any) => {
  //       if (resp.sucess) {
  //         swal.fire(resp.msg, '');
  //         // this.router.navigate(['/registration']);
  //       } else {
  //         swal.fire(resp.msg, '');
  //         // this.router.navigate(['/registration']);

  //       }
  //     },
  //     (error) => { }
  //   );
  // } catch (e) { }
}
delete_company(company) {
  const adminData: any = JSON.parse(this.cookieService.get('adminData'));
  const body = {
      admin_id: adminData.id,
      session_id: adminData.session_id,
      company_id: company.id,
      status: 2,
  };
  try {
      this.appService.postMethod('delete_company', body).subscribe(
          (resp) => {
              if (resp.success) {
                  this.List_All_Companies(0);
              } else {
              }
          },
          (error) => {}
      );
  } catch (e) {}
}

onNextPage(event) {
  this.currentPage_approve = event;
  this.approveskip = event.pageIndex * 10;
  console.log(this.approveskip, 'Skip');
  this.List_All_Companies(this.approveskip);
  return event;
}

  open(content, data) {
    this.postDataView = data;
    this.editData = data;
    // console.log(this.editData);
    // this.popupCalls(content);
    this.postdataShow = content;
    if (this.postDataView === '0') {
        let ID;
        const url = this.postDataView.video_url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
        if (url[2] !== undefined) {
            ID = url[2].split(/[^0-9a-z_\-]/i);
            this.safeSrc =  this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + ID[0]);
          } else {
            this.postDataView.videoId =  this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + url);
          }
          console.log(this.postDataView);
    }
}
close(content) {
  this.postdataShow = content;
}

popupCalls(content) {
    console.log('hllo', content);
    this.modalService.open(content).result.then(
        (result) => {
            this.closeResult = `Closed with: ${result}`;
            console.log('in close', content);
        },
        (reason) => {
            console.log('in dismis');
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
    );
}


private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
    } else {
        return `with: ${reason}`;
    }
}
}
