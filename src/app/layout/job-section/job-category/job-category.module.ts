import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { JobCategoryRoutingModule } from './job-category-routing.module';
import { JobCategoryComponent } from './job-category.component';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  declarations: [JobCategoryComponent],
  imports: [
    CommonModule,
    JobCategoryRoutingModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    TranslateModule,
    MatCardModule,
    MatSelectModule,
    MatInputModule,
    MatPaginatorModule
  ]
})
export class JobCategoryModule { }
