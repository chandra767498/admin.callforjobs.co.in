import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import {
  NgbModal,
  NgbModalConfig,
  ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from 'src/app/app.service';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

import {
  FormBuilder,
  FormGroup,
  FormArray,
  FormControl,
  ValidatorFn,
} from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
@Component({
  selector: 'app-job-category',
  templateUrl: './job-category.component.html',
  styleUrls: ['./job-category.component.scss']
})
export class JobCategoryComponent implements OnInit {
  categoryDataNew: any = {};
  paramsData: any = {};
  adminData: any;
  title = 'appBootstrap';
  model;
  closeResult: string;
  postDataView: any = {};
  safeSrc: SafeResourceUrl;
  data: any;
  pageEvent: PageEvent;
  approveskip = 0;
  currentPage_approve: any = {};
  limit = 10;
  skip = 0;
  length: number;
  postdataShow: boolean;
  categoryData: any = {};
  languageData: any;
  jobcategoryData: any = {};
  LoginForm: FormGroup;
  editData: any = {};
  constructor(
    private modalService: NgbModal,
    private sanitizer: DomSanitizer,
    private appService: AppService,
    private cookieService: CookieService,
    private router: Router,


  ) {
    if (this.cookieService.get('adminData')) {
      this.adminData = JSON.parse(this.cookieService.get('adminData'));
    }
  }
  ngOnInit(): void {
    this.jobcategoryData.status = 'active';
    this.paramsData.admin_id = this.adminData.id;
    this.paramsData.session_id = this.adminData.session_id;
    this.List_All_job_Category(0);
  }
  submitForm() {
    const data = {
      admin_id: this.paramsData.admin_id,
      session_id: this.paramsData.session_id,
      name: this.jobcategoryData.name,
      type: this.jobcategoryData.status
    };
    console.log(data);
    try {
      this.appService.postMethod('Add_job_Category', data).subscribe(
        (resp: any) => {
          if (resp.success) {
            this.jobcategoryData = resp.msg;
            swal.fire('Job Category Created Sucessfully');
            this.List_All_job_Category(0);
          } else {
            swal.fire(resp.msg, '');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }
  List_All_job_Category(skip) {
    console.log(this.paramsData);
    const data = {
      admin_id: this.paramsData.admin_id,
      session_id: this.paramsData.session_id,
      skip: skip,
      limit: 10,
    };
    try {
      this.appService.postMethod('List_All_job_Category', data).subscribe(
        (resp: any) => {
          if (resp.success) {
            this.categoryData = resp.data;
          this.length = resp.count;
            console.log(resp);
          } else {
            swal.fire(resp.msg, 'error');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }
  editForm() {
    const data = {
        admin_id :  this.paramsData.admin_id,
        session_id: this.paramsData.session_id,
        category_id: this.editData.id,
        name: this.editData.name,
        type: this.editData.status,
    };
    console.log(data);
    try {
        this.appService.postMethod('Update_job_category', data).subscribe(
            (resp: any) => {
                if (resp.success) {
                    swal.fire(resp.msg, '');
                } else {
                    swal.fire(resp.msg, '');
                }
            },
            (error) => {}
        );
    } catch (e) {}
}
  onNextPage(event) {
    console.log(event);
    this.currentPage_approve = event;
    this.approveskip = event.pageIndex * 10;
    console.log(this.approveskip, 'Skip');
    this.List_All_job_Category(this.approveskip);
    return event;
  }
  delcategory(category) {
    swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover the job Category',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      if (result.value) {
        this.delete_job_category(category);
        swal.fire(
          'Deleted!',
          'Your Job Category deleted Sucessfully.',
          'success'
        );
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal.fire('Cancelled', 'Your job Category is safe :)', 'error');
      }
    });
  }
  delete_job_category(category) {
    const adminData: any = JSON.parse(this.cookieService.get('adminData'));
    const body = {
        admin_id: adminData.id,
        session_id: adminData.session_id,
        category_id: category.id,
        status: 2,
    };
    try {
        this.appService.postMethod('delete_job_category', body).subscribe(
            (resp) => {
                if (resp.success) {
                    // this.getAllArticleList(this.page);
                    this.List_All_job_Category(0);
                } else {
                }
            },
            (error) => {}
        );
    } catch (e) {}
}

  open(content, data) {
    this.postDataView = data;
    this.editData = data;
    console.log(this.editData);
    this.editData = content;
    this.postdataShow = content;
    if (this.postDataView === '0') {
      let ID;
      const url = this.postDataView.video_url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
      if (url[2] !== undefined) {
        ID = url[2].split(/[^0-9a-z_\-]/i);
        this.safeSrc = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + ID[0]);
      } else {
        this.postDataView.videoId = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + url);
      }
      console.log(this.postDataView);
    }
  }
  close(content) {
    this.postdataShow = content;
  }
}
