import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobCategoryComponent } from './job-category.component';


const routes: Routes = [
  {
    path: '',
    component: JobCategoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobCategoryRoutingModule { }
