import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {
    NgbModal,
    NgbModalConfig,
    ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    ValidatorFn,
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from 'src/app/app.service';
import swal from 'sweetalert2';
import { lang } from 'moment';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-static-data',
  templateUrl: './static-data.component.html',
  styleUrls: ['./static-data.component.scss'],
  providers: [NgbModalConfig, NgbModal],
})
export class StaticDataComponent implements OnInit {
  isAdminListLoadin = false;
  LanguageList = [];
  data; any;
  postdataShow: boolean;
  LanguageID: any;
  skip = 0;
  limit = 10;
  languages_number: any = 0;
  postDataView: any = {};


  title = 'appBootstrap';
  model;
  closeResult: string;
  paramsData: any = {};
  adminData: any;
  languageData = [];
  staticData: any = {};
  LoginForm: FormGroup;
  EditStaticDataForm: FormGroup;
  CreateStaticData: FormGroup;
  create_static_data: any = {};
  selectedTabIndex = 0;
  static_data_count = 0;
  static_data = [];
  edit_static_data: any = {};
  safeSrc: SafeResourceUrl;

  language_id = 1;
  constructor(
    private cookieService: CookieService,
    private appService: AppService,
    private location: Location,
    // private route: ActivatedRoute,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    public router: Router,
    private sanitizer: DomSanitizer,

  ) {   if (this.cookieService.get('adminData')) {
    this.adminData = JSON.parse(this.cookieService.get('adminData'));
// } else {
//     this.adminData = {};
//     this.router.navigate(['/login']);
}
// if (!this.adminData.is_admin) {
    // if (this.adminData.permissions) {
    //   console.log('Admin');
    //   if (!this.adminData.permissions.Static_Data) {
    //     this.router.navigate(['/dashboard']);
    //   }
    // }
//   }
 }

  ngOnInit(): void {
    this.paramsData.admin_id = this.adminData.id;
    this.paramsData.session_id = this.adminData.session_id;
    this.List_All_Languages(this.paramsData);
    this.view_static_data(1);
  }
  selectedIndexChange(index: number) {
    setTimeout(() => this.selectedTabIndex = index);
    if (index === 0) {
        this.language_id = 1;
        this.view_static_data(this.language_id);

      }
    if (index === 1) {
        this.language_id = 2;
        this.view_static_data(this.language_id);
    }
    if (index === 2) {
      console.log('Telugu');
      this.language_id = 3;
      this.view_static_data(this.language_id);

      // this.Employee_List(2);
    }
    if (index === 3) {
      console.log('Tamil');
      this.language_id = 4;
      this.view_static_data(this.language_id);
    }
    if (index === 4) {
        console.log('Kannada');
        this.language_id = 5;
        this.view_static_data(this.language_id);
      }
      if (index === 5) {
        console.log('Odiya');
        this.language_id = 6;
        this.view_static_data(this.language_id);
      }
  }

  open(content, data) {
    this.postDataView = data;
    this.edit_static_data = data;

    console.log(this.edit_static_data);
    // this.popupCalls(content);
    this.postdataShow = content;
    if (this.postDataView === '4') {
        let ID;
        const url = this.postDataView.video_url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
        if (url[2] !== undefined) {
            ID = url[2].split(/[^0-9a-z_\-]/i);
            this.safeSrc =  this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + ID[0]);
          } else {
            this.postDataView.videoId =  this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + url);
          }
          console.log(this.postDataView);
    }
}
close(content) {
    this.postdataShow = content;
  }

  view_static_data(Language_ID) {
      const data = {
          admin_id : this.paramsData.admin_id,
          session_id : this.paramsData.session_id,
          language_id: this.language_id
      };
    try {
        this.static_data = [];

        this.appService.postMethod('Admin_Fetch_Static_Data', data).subscribe(
            (resp: any) => {
                if (resp.success) {
                    this.static_data_count = resp.count;
                    this.static_data = resp.data;
                    console.log(this.static_data);

                } else {
                    this.static_data = [];
                    swal.fire(resp.msg, 'Oops!! No Data');
                }
            },
            (error) => {}
        );
    } catch (e) {}
}

// Edit Static Data
edit_staticData() {
    const data = {
        admin_id : this.paramsData.admin_id,
        session_id : this.paramsData.session_id,
        static_id : this.edit_static_data.static_id,
        language_id : this.language_id,
        title : this.edit_static_data.title,
        name : this.edit_static_data.name
    };
  try {
      this.appService.postMethod('Edit_Static_Language', data).subscribe(
          (resp: any) => {
              if (resp.sucess) {
                this.ngOnInit();
                  swal.fire(resp.msg, 'Success');
              } else {
                  swal.fire(resp.msg, 'Error');
              }
          },
          (error) => {}
      );
  } catch (e) {}




}


// Create Static Data
create_staticData() {
    console.log(this.create_static_data, 'Create Data');
    const data = {
        admin_id : this.paramsData.admin_id,
        session_id : this.paramsData.session_id,
        static_id : this.create_static_data.static_id,
        language_id : this.create_static_data.language_id,
        title : this.create_static_data.title,
        name : this.create_static_data.name
    };
    console.log(data);
  try {
      this.appService.postMethod('Add_Static_Language', data).subscribe(
          (resp: any) => {
              if (resp.sucess) {
                  swal.fire(resp.msg, 'Success');
              } else {
                  swal.fire(resp.msg, 'Error');
              }
          },
          (error) => {}
      );
  } catch (e) {}
}


List_All_Languages(data) {
    try {
        this.appService.postMethod('List_All_Languages', data).subscribe(
            (resp: any) => {
                if (resp.success) {
                    this.languageData = resp.data;
                    console.log(this.languageData, );
                } else {
                    swal.fire(resp.msg, 'error');
                }
            },
            (error) => {}
        );
    } catch (e) {}
}

submitForm() {
    const data = {
        admin_id: this.paramsData.admin_id,
        session_id: this.paramsData.session_id,
        language_id: this.staticData.language_id,
        static_id: this.staticData.static_id,
        name: this.staticData.static_name,
        title: this.staticData.title,

    };
    console.log(data);
    try {
        this.appService.postMethod('Edit_Category', data).subscribe(
            (resp: any) => {
                if (resp.success) {
                    this.staticData = resp.msg;
                    console.log(resp);
                } else {
                    swal.fire(resp.msg, '');
                }
            },
            (error) => {}
        );
    } catch (e) {}
}

// create(content) {
//     this.popupCalls(content);

// }

// popupCalls(content) {
//   console.log('hllo', content);
//   this.modalService.open(content).result.then(
//       (result) => {
//           this.closeResult = `Closed with: ${result}`;
//         //   console.log('in close', content);
//       },
//       (reason) => {
//           this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
//       }
//   );
// }


delstatic(index) {
    swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover the static data',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      if (result.value) {
        this.Delete_static_data(index);
        swal.fire(
          'Deleted!',
          'Your static data deleted Sucessfully.',
          'success'
        );
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal.fire('Cancelled', 'Your static data is safe :)', 'error');
      }
    });
  }
  Delete_static_data(index) {
    const adminData: any = JSON.parse(this.cookieService.get('adminData'));
    const body = {
        admin_id: adminData.id,
        session_id: adminData.session_id,
        // category_id: category.id,
        status: 2,
    };
    try {
        this.appService.postMethod('Delete_static_data', body).subscribe(
            (resp) => {
                if (resp.success) {
                    // this.getAllArticleList(this.page);
                    // this.List_All_job_Category(0);
                } else {
                }
            },
            (error) => {}
        );
    } catch (e) {}
}



private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
  } else {
      return `with: ${reason}`;
  }
}

}
