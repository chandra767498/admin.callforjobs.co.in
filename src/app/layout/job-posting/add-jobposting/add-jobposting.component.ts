import { Pipe, PipeTransform, Component, OnInit } from '@angular/core';
import {
    NgbModal,
    NgbModalConfig,
    ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { CookieService } from 'ngx-cookie-service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-add-jobposting',
  templateUrl: './add-jobposting.component.html',
  styleUrls: ['./add-jobposting.component.scss'],
  providers: [NgbModalConfig, NgbModal],
})
export class AddJobpostingComponent implements OnInit {
  jobData: any = {};
  StateList = [];
  filter: any = {};
  CountryList = [];
  CityList = [];
  dateshow: boolean;
  minDate = new Date();
  paramsData: any = {};
  dateFilters: any = {};
  state_id: any = {};
  adminData: any;
  // public minDate: Date = new Date('05/07/2015');
  public maxDate: Date = new Date();
  public value: Date = this.maxDate;
  public value1: Date = this.maxDate;
  start_date: any;
  end_date: any;
  constructor(
    private cookieService: CookieService,
    private appService: AppService,
  ) {
    if (this.cookieService.get('adminData')) {
        this.adminData = JSON.parse(this.cookieService.get('adminData'));
     }
  }


  ngOnInit(): void {
    // this.jobData.company = 'anynews';
    this.jobData.jobskills = '0';
    this.filter.state_id = '0';
    this.filter.city_id = '0';
    this.jobData.Peroid = '0';
    this.jobData.categories = '0';
    this.jobData.type = '0';
    this.jobData.gender = '0';
    this.jobData.education = '0';
    this.jobData.experience = '0';
    this.jobData.status = 'active';
   this.jobData.country = '0';
   this.paramsData.admin_id = this.adminData.id;
   this.paramsData.session_id = this.adminData.session_id;
   this. getStatesList();
   this.getCitiesList(this.state_id);
  }
  submitForm() {
    const data = {
      admin_id : this.paramsData.admin_id,
      session_id: this.paramsData.session_id,
      company: this.jobData.company,
      title: this.jobData.title,
      description: this.jobData.description,
      email: this.jobData.email,
      job_contact: this.jobData.contact,
      job_skills: this.jobData.jobskills,
      country_id: this.jobData.country,
      state_id: this.filter.state_id,
      city_id: this.filter.city_id,
      salaryfrom: this.jobData.salaryfrom,
      salaryto: this.jobData.salaryto,
      period: this.jobData.Peroid,
      categories: this.jobData.categories,
      type: this.jobData.type,
      position: this.jobData.position,
      gender: this.jobData.gender,
      date: this.dateFilters.date,
      education: this.jobData.education,
      experience: this.jobData.experience,
      status: this.jobData.status,
  };
  console.log(data);
  try {
      this.appService.postMethod('add_job_posting', data).subscribe(
          (resp: any) => {
              if (resp.success) {
                  this.jobData = resp.msg;
                  swal.fire('Job Posting Added Sucessfully');
                  console.log(resp);
              } else {
                  swal.fire(resp.msg, '');
              }
          },
          (error) => {}
      );
  } catch (e) {}
  }
  getStatesList() {
    // this.isAdminListLoadin = true;
    const adminData = JSON.parse(this.cookieService.get('adminData'));
    const data = {
        admin_id: adminData.id,
        session_id: adminData.session_id,
        // language_id: parseInt(this.language_id),
    };
    console.log('Request Body For STATES', data);
    try {
        this.appService.postMethod('view_states', data).subscribe(
            (resp: any) => {
                if (resp.success) {
                    this.StateList = resp.data;
                    console.log(resp);
                } else {
                    swal.fire(resp.msg, 'error');
                }
            },
            (error) => {}
        );
    } catch (e) {}
}
getCitiesList(state_id) {
    // this.isAdminListLoadin = true;
    const adminData = JSON.parse(this.cookieService.get('adminData'));
    const data = {
        admin_id: adminData.id,
        session_id: adminData.session_id,
        state_id: this.filter.state_id,

    };
    console.log('Request Body For CITIES', data);
    try {
        this.appService.postMethod('view_cities', data).subscribe(
            (resp: any) => {
                if (resp.success) {
                    this.CityList = resp.data;
                    console.log(resp);
                } else {
                    swal.fire(resp.msg, 'error');
                }
            },
            (error) => {}
        );
    } catch (e) {}
}
changeType(value) {
  if (value === 'timebase') {
      this.dateshow = true;
  } else {
      this.dateshow = false;
  }
}
dateChanged() {
  this.value = new Date(this.value);
  this.value1 = new Date(this.value1);
  this.start_date = this.value.getFullYear() + '-' +  (this.value.getMonth() + 1) + '-' + this.value.getDate();
  this.end_date = this.value1.getFullYear() +  '-' +  (this.value1.getMonth() + 1) + '-' +   this.value1.getDate();
}
cities(value) {
    console.log(value);
    console.log('City Status');
}
states() {
    // console.log(states);
    console.log('State Status');
    console.log('THis is State', this.filter.state_id);
    this.getCitiesList(this.filter.state_id);
}
view_cities(state) {
    const data = {
        admin_id: this.paramsData.admin_id,
        session_id: this.paramsData.session_id,
        // language_id: this.adminData.language_id,
    };
    console.log(data);
    try {
        this.appService.postMethod('view_cities', data).subscribe(
            (resp: any) => {
                if (resp.success) {
                    this.cities = resp.data;
                    console.log(this.cities, 'Deepika');
                } else {
                    swal.fire(resp.msg, '');
                }
            },
            (error) => {}
        );
    } catch (e) {}
}
}
