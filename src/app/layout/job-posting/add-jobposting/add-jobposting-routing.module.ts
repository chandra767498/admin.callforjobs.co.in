import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddJobpostingComponent } from './add-jobposting.component';


const routes: Routes = [{
  path: '',
  component: AddJobpostingComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddJobpostingRoutingModule { }
