import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { JobPostingRoutingModule } from './job-posting-routing.module';
import { JobPostingComponent } from './job-posting.component';
import { EditJobpostingComponent } from './edit-jobposting/edit-jobposting.component';


@NgModule({
  declarations: [JobPostingComponent, EditJobpostingComponent],
  imports: [
    CommonModule,
    JobPostingRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    FlexLayoutModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatTableModule,
    MatInputModule
  ]
})
export class JobPostingModule { }
