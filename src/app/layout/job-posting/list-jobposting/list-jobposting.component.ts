import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import {
  NgbModal,
  NgbModalConfig,
  ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import { PageEvent } from '@angular/material/paginator';
import swal from 'sweetalert2';
@Component({
  selector: 'app-list-jobposting',
  templateUrl: './list-jobposting.component.html',
  styleUrls: ['./list-jobposting.component.scss']
})
export class ListJobpostingComponent implements OnInit {
  jobData: any = {};
  paramsData: any = {};
  adminData: any;
  page: number;

  data: any = {};
  pageEvent: PageEvent;
  approveskip = 0;
  currentPage_approve: any = {};
  limit = 10;
  skip = 0;
  length: number;
  constructor(  private router: Router,
    private appService: AppService,
    private cookieService: CookieService,
    private modalService: NgbModal) {
      if (this.cookieService.get('adminData')) {
        this.adminData = JSON.parse(this.cookieService.get('adminData'));
     }
     }

  ngOnInit(): void {
    this.paramsData.admin_id = this.adminData.id;
    this.paramsData.session_id = this.adminData.session_id;
    this.Get_Job_Posting_Details(0);

  }
  Get_Job_Posting_Details(skip) {
    console.log(this.paramsData);
    const data = {
      admin_id: this.paramsData.admin_id,
      session_id: this.paramsData.session_id,
      skip: skip,
      limit: 10,
    };
    try {
      this.appService.postMethod('Get_Job_Posting_Details', data).subscribe(
        (resp: any) => {
          if (resp.success) {
            this.jobData = resp.data;
          this.length = resp.count;
            // this.length = resp.count;
            console.log(resp);
          } else {
            swal.fire(resp.msg, 'error');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }
  onNextPage(event) {
    console.log(event);
    this.currentPage_approve = event;
    this.approveskip = event.pageIndex * 10;
    console.log(this.approveskip, 'Skip');
    this.Get_Job_Posting_Details(this.approveskip);
    return event;
  }


  Active_Inactive_job_posting(data) {
    const adminData: any = JSON.parse(this.cookieService.get('adminData'));
    const body = {
        admin_id: adminData.id,
        session_id: adminData.session_id,
        posting_id: data.id,
        status: 2,
    };
    try {
        this.appService.postMethod('Active_Inactive_job_posting', body).subscribe(
            (resp) => {
                if (resp.success) {
                  this.Get_Job_Posting_Details(0);
                    this.ngOnInit();
                } else {
                }
            },
            (error) => {}
        );
    } catch (e) {}
  }

  EditJobPosting(event, data) {
    console.log(data);
    this.router.navigate(['job-posting/edit-jobposting'], data);
  }
  delposting(data) {

    swal.fire({
        title: 'Are you sure?',
        text: 'You want to delete',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it',
    }).then((result) => {
        if (result.value) {
          this.Active_Inactive_job_posting(data);
            swal.fire(
                'Deleted!',
                'Job Posting deleted Sucessfully.',
                'success'
            );
            // this.delForm();
        } else if (result.dismiss === swal.DismissReason.cancel) {
            swal.fire('Cancelled', 'Your Employee is safe :)', 'error');
        }
    });
  }
}
