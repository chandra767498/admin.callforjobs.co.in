import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListJobpostingComponent } from './list-jobposting.component';


const routes: Routes = [{
  path:'',
  component:ListJobpostingComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListJobpostingRoutingModule { }
