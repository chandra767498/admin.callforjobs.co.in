import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListJobpostingComponent } from './list-jobposting.component';

describe('ListJobpostingComponent', () => {
  let component: ListJobpostingComponent;
  let fixture: ComponentFixture<ListJobpostingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListJobpostingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListJobpostingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
