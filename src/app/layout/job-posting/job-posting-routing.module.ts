import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'add-jobposting',
  },
  {
    path: 'add-jobposting',
    loadChildren: () =>
      import('./add-jobposting/add-jobposting.module').then(
        (m) => m.AddJobpostingModule
      ),
  },
  
  {
    path: 'list-jobposting',
    loadChildren: () =>
      import('./list-jobposting/list-jobposting.module').then((m) => m.ListJobpostingModule),
  },
  {
    path: 'edit-jobposting',
    loadChildren: () =>
      import('./edit-jobposting/edit-jobposting.module').then((m) => m.EditJobpostingModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobPostingRoutingModule { }
