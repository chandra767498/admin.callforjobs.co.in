import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditJobpostingComponent } from './edit-jobposting.component';


const routes: Routes = [{
  path: '',
  component: EditJobpostingComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditJobpostingRoutingModule { }
