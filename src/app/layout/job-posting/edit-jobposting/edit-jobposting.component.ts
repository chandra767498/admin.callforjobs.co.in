import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import swal from 'sweetalert2';
import { AppService } from 'src/app/app.service';
@Component({
  selector: 'app-edit-jobposting',
  templateUrl: './edit-jobposting.component.html',
  styleUrls: ['./edit-jobposting.component.scss']
})
export class EditJobpostingComponent implements OnInit {
  adminData: any;
  paramsData: any = [];
  email_id: '';
  employee_id;
  editData: any = {};
  is_admin = '';
  jobData: any = {};
  StateList = [];
  filter: any = {};
  CountryList = [];
  CityList = [];
  dateshow: boolean;
  minDate = new Date();
  dateFilters: any = {};
  hide = true;
  registrationData: any = {};
  permissionsData: any = {};
  item: any = {};
  constructor( private router: Router,
    private appService: AppService,
    private cookieService: CookieService,
  ) {
    if (this.cookieService.get('adminData')) {
      this.adminData = JSON.parse(this.cookieService.get('adminData'));
  } }

  ngOnInit(): void {
    this.jobData.jobskills = '0';
    this.filter.state_id = '0';
    this.filter.city_id = '0';
    this.jobData.Peroid = '0';
    this.jobData.categories = '0';
    this.jobData.type = '0';
    this.jobData.gender = '0';
    this.jobData.education = '0';
    this.jobData.experience = '0';
    this.jobData.status = 'active';
   this.jobData.country = '0';
   this.paramsData.admin_id = this.adminData.id;
   this.paramsData.session_id = this.adminData.session_id;
  }
  editForm() {
    const data = {
        admin_id: this.paramsData.admin_id,
        session_id: this.paramsData.session_id,
        email_id: this.email_id,
        name: this.jobData.name,
        employee_id: this.item.id,
        permissions: this.permissionsData

    };
    console.log(data);
    try {
      this.appService.postMethod('edit_employee', data).subscribe(
          (resp: any) => {
              if (resp.sucess) {
                  swal.fire(resp.msg, '');
                  // this.router.navigate(['/add-employee']);
              } else {
                  swal.fire(resp.msg, '');
                  // this.router.navigate(['/add-employee']);

              }
          },
          (error) => {}
      );
  } catch (e) {}
}

}
