import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditJobpostingComponent } from './edit-jobposting.component';

describe('EditJobpostingComponent', () => {
  let component: EditJobpostingComponent;
  let fixture: ComponentFixture<EditJobpostingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditJobpostingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditJobpostingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
