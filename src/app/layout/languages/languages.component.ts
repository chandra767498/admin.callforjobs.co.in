import { Location } from '@angular/common';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import {
    NgbModal,
    NgbModalConfig,
    ModalDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    ValidatorFn,
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from 'src/app/app.service';
import swal from 'sweetalert2';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.scss'],
  providers: [NgbModalConfig, NgbModal],
})
export class LanguagesComponent implements OnInit {
  title = 'appBootstrap';
  model;
  closeResult: string;
  paramsData: any = {};
  adminData: any;
  postDataView: any = {};
  safeSrc: SafeResourceUrl;
  data: any;
  postdataShow: boolean;
  languageData: any;
  addLanguageData: any = {};
  LoginForm: FormGroup;
  editData: any = {};
  constructor(
    private cookieService: CookieService,
    private appService: AppService,
    private location: Location,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private sanitizer: DomSanitizer,
    public router: Router
  ) {  if (this.cookieService.get('adminData')) {
    this.adminData = JSON.parse(this.cookieService.get('adminData'));
// } else {
//     this.adminData = {};
//     this.router.navigate(['/login']);
// }
// if (!this.adminData.is_admin) {
//     if (this.adminData.permissions) {
//       console.log('Admin');
//       if (!this.adminData.permissions.Languages) {
//         this.router.navigate(['/dashboard']);
//       }
//     }
  }
}
  ngOnInit(): void {
    this.paramsData.admin_id = this.adminData.id;
    this.paramsData.session_id = this.adminData.session_id;
    this.List_All_Languages(this.paramsData);
  }
  List_All_Languages(data) {
    try {
        this.appService.postMethod('List_All_Languages', data).subscribe(
            (resp: any) => {
                if (resp.success) {
                    console.log(resp);
                    this.languageData = resp.data;
                } else {
                    swal.fire(resp.msg, 'error');
                }
            },
            (error) => { }
        );
    } catch (e) { }
}

submitForm() {
    const data = {
        admin_id : this.adminData.id,
        session_id: this.adminData.session_id,
        title: this.addLanguageData.title,
        name: this.addLanguageData.name,
        type: this.addLanguageData.status,
    };
    console.log(data);
    try {
        this.appService.postMethod('Add_Language', data).subscribe(
            (resp: any) => {
                if (resp.success) {
                    this.addLanguageData = resp.msg;
                    swal.fire('Language Added Succesfully');

                } else {
                    swal.fire(resp.msg, '');
                }
            },
            (error) => {}
        );
    } catch (e) {}
}

editForm() {
    const data = {
        admin_id : this.adminData.id,
        session_id: this.adminData.session_id,
        language_id: this.editData.id,
        title: this.editData.title,
        name: this.editData.name,
        // type: this.editData.status,
    };
    console.log(data);
    try {
        this.appService.postMethod('Update_Language', data).subscribe(
            (resp: any) => {
                if (resp.success) {
                    swal.fire(resp.msg, '');
                } else {
                    swal.fire(resp.msg, '');
                }
            },
            (error) => {}
        );
    } catch (e) {}
}

// openAdd(content) {
//     this.popupCalls(content);
// }

  open(content, data) {
    this.postDataView = data;
    this.editData = data;
    console.log(this.editData);
    // this.popupCalls(content);
    this.postdataShow = content;
    if (this.postDataView === '0') {
        let ID;
        const url = this.postDataView.video_url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
        if (url[2] !== undefined) {
            ID = url[2].split(/[^0-9a-z_\-]/i);
            this.safeSrc =  this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + ID[0]);
          } else {
            this.postDataView.videoId =  this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + url);
          }
          console.log(this.postDataView);
    }
}

close(content) {
    this.postdataShow = content;
  }
// popupCalls(content) {
//     console.log('hllo', content);
//     this.modalService.open(content).result.then(
//         (result) => {
//             this.closeResult = `Closed with: ${result}`;
//             console.log('in close', content);
//         },
//         (reason) => {
//             console.log('in dismis');
//             this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
//         }
//     );
// }

// private getDismissReason(reason: any): string {
//     if (reason === ModalDismissReasons.ESC) {
//         return 'by pressing ESC';
//     } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
//         return 'by clicking on a backdrop';
//     } else {
//         return `with: ${reason}`;
//     }
// }

}
