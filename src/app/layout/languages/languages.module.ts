import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';


import { LanguagesRoutingModule } from './languages-routing.module';
import { LanguagesComponent } from './languages.component';


@NgModule({
  declarations: [LanguagesComponent],
  imports: [
    CommonModule,
    LanguagesRoutingModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    TranslateModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class LanguagesModule { }
