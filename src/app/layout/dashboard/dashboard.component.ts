import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from 'src/app/app.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  adminData: any;
  userdata: any = {};
  items: any = [];
  paramsData: any = [];
  language_id;
  isEdit: any = {};
  optionValue = '';
  optionalValue = '';
  email_id: '';
  approveskip = 0;
  limit = 10;
  pageEvent: PageEvent;
  skip = 0;
  length: number;
  employee_id;
  registrationData: any = {};
  currentPage_approve: any = {};
  permissionsData: any = {};
  delete_employee_data: any = {};
  delete_employee: any = {};
  closeResult: string;
  pageIndex: number;
  pageSize: number;
  constructor(
    private cookieService: CookieService,
    private appService: AppService,
    public router: Router,
  ) {

    if (this.cookieService.get('adminData')) {
      this.adminData = JSON.parse(this.cookieService.get('adminData'));
    }

  }
  ngOnInit() {
    this.paramsData.admin_id = this.adminData.id;
    this.paramsData.session_id = this.adminData.session_id;
    // this.List_All_Languages(this.paramsData);
    this.List_All_Employees(0);
  }

  List_All_Employees(skip) {
    // console.log(this.employeeData);
    const adminData: any = JSON.parse(this.cookieService.get('adminData'));
    const data = {
      admin_id: this.paramsData.admin_id,
      session_id: this.paramsData.session_id,
      skip: skip,
      limit: 10,
    };
    try {
      this.appService.postMethod('List_All_Employees', data).subscribe(
        (resp: any) => {
          if (resp.success) {
            this.items = resp.data;
            // this.length = resp.count;
            this.length = resp.count;
            console.log(resp);
          } else {
            swal.fire(resp.msg, 'error');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }
  onNextPage(event) {
    this.currentPage_approve = event;
    this.approveskip = event.pageIndex * 10;
    console.log(this.approveskip, 'Skip');
    this.List_All_Employees(this.approveskip);
    return event;
  }
}
