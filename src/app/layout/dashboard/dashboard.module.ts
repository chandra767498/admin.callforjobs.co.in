import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';
// import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';
import {MatSelectModule} from '@angular/material/select';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { ChartsModule } from 'ng2-charts';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';


@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    // DateRangePickerModule,
    ChartsModule,
    // DatePickerModule,
    MatCardModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatPaginatorModule,
    FormsModule,
    MatSelectModule,
    FlexLayoutModule.withConfig({ addFlexToParent: false })
  ]
})
export class DashboardModule { }
